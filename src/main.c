#include <stdbool.h>

#include "mem.h"
#include "mem_internals.h"
#include "tests.h"

struct test {
    bool (*tester)( void const* heap );
    const char* const test_name;
};

static struct test tests[] = {
        { .tester = successful_allocating, .test_name = "Allocating one block" },
        { .tester = successful_allocating_few_blocks_and_freeing_one, .test_name = "Allocating few blocks and freeing one" },
        { .tester = successful_allocating_and_freeing_few_blocks, .test_name = "Allocating and freeing few blocks" },
        { .tester = allocating_new_block_extending_old_block, .test_name = "Growing heap extending previous heap" },
        { .tester = allocating_new_block, .test_name = "Growing heap at other place" }
};

int main() {
    void* heap = heap_init(1000);

    for ( size_t i = 0; i < 5; ++i ) {
        fprintf(stdout, "\n--- Test #%zu: %s ---\n", i + 1, tests[i].test_name);

        if ( tests[i].tester(heap) )
            fprintf(stdout, "\nTest #%zu passed\n", i + 1);
        else {
            fprintf(stdout, "\nTest #%zu failed\n", i + 1);

            return 1;
        }
    }

    fprintf(stdout, "\nAll tests successfully passed\n");

    return 0;
}
