
#ifndef _TESTS_H_
#define _TESTS_H_

#include <stdbool.h>

#include "mem.h"

bool successful_allocating( void const* heap );

bool successful_allocating_few_blocks_and_freeing_one( void const* heap );

bool successful_allocating_and_freeing_few_blocks( void const* heap );

bool allocating_new_block_extending_old_block( void const* heap );

bool allocating_new_block( void const* heap );

#endif
