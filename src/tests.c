
#define _DEFAULT_SOURCE

#include <sys/mman.h>

#include "mem_internals.h"
#include "tests.h"

static struct block_header* last_block( void const* heap ) {
	struct block_header* block = (struct block_header*) heap;
	
	while ( block->next != NULL )
		block = block->next;
	
	return block;
}

static void malloc_all( void const* heap ) {
	struct block_header* block = (struct block_header*) heap;
	
	while ( block != NULL ) {
		if ( block->is_free )
			_malloc(block->capacity.bytes);
			
		block = block->next;
	}
}

static void free_all( void const* heap ) {
	struct block_header* block = (struct block_header*) heap;
	
	while ( block != NULL ) {
		_free(block->contents);	
		block = block->next;
	}
}

static struct block_header* get_header( void* addr ) {
	return (struct block_header*) ( ((uint8_t*) addr) - offsetof(struct block_header, contents) );
}

static bool blocks_continuous( struct block_header* block1, struct block_header* block2 ) {
	return (void*) (block1->contents + block1->capacity.bytes) == ((void*) block2);
}

static bool check_blocks( void* addr1, void* addr2 ) {
	return get_header(addr1)->next == get_header(addr2);
}

/* Test #1 */
bool successful_allocating( void const* heap ) {
	fprintf(stderr, "\nStart heap condition:\n");
	debug_heap(stderr, heap);

	void* addr = _malloc(100);
	
	if ( addr != NULL) {
		fprintf(stdout, "\nChecking: addr != NULL - completed\n");
		
		fprintf(stderr, "\nHeap condition after allocating block:\n");	
		debug_heap(stderr, heap);
		
		_free(addr);
		
		return true;
	}
	else
		fprintf(stdout, "\nChecking: addr != NULL - failed\n");
	
	return false;
}

/* Test #2 */
bool successful_allocating_few_blocks_and_freeing_one( void const* heap ) {
	fprintf(stderr, "\nStart heap condition:\n");
	debug_heap(stderr, heap);
	
	void* addr1 = _malloc(100);
	
	if ( addr1 != NULL) {
		fprintf(stdout, "\nChecking: addr1 != NULL - completed\n");
	
		void* addr2 = _malloc(100);
		
		if ( addr2 != NULL) {
			fprintf(stdout, "Checking: addr2 != NULL - completed\n");
		
			if ( check_blocks(addr1, addr2) ) {
				fprintf(stdout, "Checking: block1->next == block2 - completed\n");
				
				fprintf(stderr, "\nHeap condition after allocating blocks:\n");
				debug_heap(stderr, heap);
				
				_free(addr1);
				
				if ( get_header(addr1)->is_free == true ) {
					fprintf(stdout, "\nChecking: block1->is_free == true - completed\n");
				
					fprintf(stderr, "\nHeap condition after freeing block:\n");
					debug_heap(stderr, heap);
					
					free_all(heap);
					
					return true;
				}
				else
					fprintf(stdout, "\nChecking: block1->is_free == true - completed\n");
			}
			else
				fprintf(stdout, "Checking: block1->next == block2 - failed\n");
		}
		else
			fprintf(stdout, "Checking: addr2 != NULL - failed\n");
		
		free_all(heap);
	}
	else
		fprintf(stdout, "\nChecking: addr1 != NULL - failed\n");
	
	return false;
}

/* Test #3 */
bool successful_allocating_and_freeing_few_blocks( void const* heap ) {
	fprintf(stderr, "\nStart heap condition:\n");
	debug_heap(stderr, heap);
	
	void* addr1 = _malloc(100);
	
	if ( addr1 != NULL) {
		fprintf(stdout, "\nChecking: addr1 != NULL - completed\n");
		
		void* addr2 = _malloc(100);
		
		if ( addr2 != NULL ) {
			fprintf(stdout, "Checking: addr2 != NULL - completed\n");
			
			if ( check_blocks(addr1, addr2) ) {
				fprintf(stdout, "Checking: block1->next == block2 - completed\n");
				
				fprintf(stderr, "\nHeap condition after allocating blocks:\n");
				debug_heap(stderr, heap);
				
				_free(addr1);
				
				if ( get_header(addr1)->is_free == true ) {
					fprintf(stdout, "\nChecking: block1->is_free == true - completed\n");
					
					_free(addr2);
				
					if ( get_header(addr2)->is_free == true ) {
						fprintf(stdout, "Checking: block2->is_free == true - completed\n");
					
						fprintf(stderr, "\nHeap condition after freeing blocks:\n");
						debug_heap(stderr, heap);
						
						return true;
					}
					else
						fprintf(stdout, "Checking: block2->is_free == true - failed\n");
				}
				else
					fprintf(stdout, "\nChecking: block1->is_free == true - failed\n");
			}
			else
				fprintf(stdout, "Checking: block1->next == block2 - failed\n");
		}
		else
			fprintf(stdout, "Checking: addr2 != NULL - failed\n");
		
		free_all(heap);
	}
	else
		fprintf(stdout, "\nChecking: addr1 != NULL - failed\n");
	
	return false;
}

/* Test #4 */
bool allocating_new_block_extending_old_block( void const* heap ) {	
	malloc_all(heap);
	struct block_header* block = last_block(heap);
	
	fprintf(stderr, "\nStart heap condition:\n");
	debug_heap(stderr, heap);
	
	void* addr = _malloc(100);
	
	if ( addr != NULL) {
		fprintf(stdout, "\nChecking: addr != NULL - completed\n");
	
		fprintf(stderr, "\nHeap condition after growing heap and allocating block:\n");
		debug_heap(stderr, heap);
	
		if ( block->next == get_header(addr) ) {
			fprintf(stdout, "\nChecking: last_block->next == block - completed\n");
			
			if ( blocks_continuous(block, get_header(addr)) ) {
				fprintf(stdout, "Checking: block continues last_block - completed\n");
				
				free_all(heap);
				
				return true;
			}
			else
				fprintf(stdout, "Checking: block continues last_block - failed\n");
		}
		else
			fprintf(stdout, "\nChecking: last_block->next == block - failed\n");
	}
	else
		fprintf(stdout, "\nChecking: addr != NULL - failed\n");

	free_all(heap);
	
	return false;
}

/* Test #5 */
bool allocating_new_block( void const* heap ) {
	malloc_all(heap);

	fprintf(stderr, "\nStart heap condition:\n");
	debug_heap(stderr, heap);
	
	struct block_header* block = last_block(heap);
	
	void* addr = block->contents + block->capacity.bytes;
	void* mmap_addr = mmap(addr, 100, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);
	
	if ( mmap_addr == addr) {
		fprintf(stdout, "\nChecking mapped_addr == block_after(last_block) - completed\n");
		
		void* new_addr = _malloc(1000);
		
		if ( get_header(new_addr) != mmap_addr ) {
			fprintf(stdout, "Checking block doesn't continues last_block - completed\n");
		
			if ( block->next == get_header(new_addr) ) {
				fprintf(stdout, "Checking last_block->next == block - completed\n");
			
				fprintf(stderr, "\nHeap condition after growing heap and allocating block at other place:\n");
				debug_heap(stderr, heap);
				
				return true;
			}
			else
				fprintf(stdout, "Checking last_block->next == block - failed\n");
		}
		else
			fprintf(stdout, "Checking block doesn't continues last_block - failed\n");
	}
	else
		fprintf(stdout, "\nChecking mapped_addr == block_after(last_block) - failed\n");
	
	free_all(heap);
	
	return true;
}
